PKGNAME=mesa+32
PKGSEC=x11
PKGDEP="aosc-aaa+32 llvm+32 x11-lib+32 libdrm+32 expat+32 eudev+32 \
        elfutils+32 libvdpau+32 libglvnd+32 libva+32 spirv-llvm-translator+32 \
        libunwind+32 lm-sensors+32 wayland+32 libcl+32 libclc+32"
BUILDDEP="devel-base+32 glslang mako pyyaml rust-bindgen rustc+32 \
          spirv-tools+32 cbindgen"
PKGDES="Runtime library for OpenGL, Vulkan, OpenCL, and more (32-bit x86 runtime)"

PKGBREAK="wine<=1.7.54"

AB_FLAGS_O3=1

# FIXME: LTO causes graphical artifacts on AMD GPUs.
NOLTO=1
ABHOST=optenv32

# Note: Intel Ray Tracing is only supported for x86_64.
# Note: RustICL was disabled, as there is no useful driver for x86 yet.
MESON_AFTER=(
    '-Ddri-drivers-path=/opt/32/lib/xorg/modules/dri'
    '-Db_ndebug=true'
    '-Dplatforms=x11,wayland'
    '-Dvulkan-layers=device-select,intel-nullhw,overlay'
    '-Degl=enabled'
    '-Dgallium-extra-hud=true'
    '-Dgallium-nine=true'
    '-Dgallium-va=enabled'
    '-Dgallium-vdpau=enabled'
    '-Dgallium-xa=enabled'
    '-Dgbm=enabled'
    '-Dgles1=enabled'
    '-Dgles2=enabled'
    '-Dglvnd=enabled'
    '-Dglx=dri'
    '-Dllvm=enabled'
    '-Dlmsensors=disabled'
    '-Dosmesa=true'
    '-Dshared-glapi=enabled'
    '-Dvalgrind=disabled'
    '-Db_lto=false'
    '-Dgallium-drivers=r300,r600,radeonsi,nouveau,virgl,swrast,svga,iris,i915,crocus,zink'
    '-Dvulkan-drivers=amd,intel,intel_hasvk,swrast'
    '-Dsse2=true'
    '-Dvulkan-icd-dir=/usr/share/vulkan/icd.d'
    '-Dlmsensors=enabled'
    '-Dgallium-opencl=icd'
    '-Dgallium-rusticl=false'
    '-Dopencl-spirv=true'
    '-Dintel-clc=enabled'
)
